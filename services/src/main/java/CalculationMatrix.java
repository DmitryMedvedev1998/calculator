import interfaces.TransitionMatrix;

import java.util.HashMap;
import java.util.Map;

public class CalculationMatrix implements TransitionMatrix<CalculationState> {

    public CalculationState getStartState(){
        return CalculationState.START;
    }

    public CalculationState getFinishState(){
        return CalculationState.FINISH;
    }

   private static final Map<CalculationState,CalculationState[]> matrix = new HashMap<CalculationState,
           CalculationState[]>(){ {

       put(CalculationState.START, new CalculationState[]{CalculationState.NUMBER, CalculationState.OPENING_BRACKET});

       put(CalculationState.NUMBER, new CalculationState[]{CalculationState.BINARY_OPERATOR,
               CalculationState.CLOSING_BRACKET, CalculationState.FINISH});

       put(CalculationState.BINARY_OPERATOR, new CalculationState[]{CalculationState.NUMBER,
               CalculationState.OPENING_BRACKET, CalculationState.FINISH});

       put(CalculationState.OPENING_BRACKET, new CalculationState[]{CalculationState.NUMBER,
               CalculationState.OPENING_BRACKET});

       put(CalculationState.CLOSING_BRACKET, new CalculationState[]{CalculationState.BINARY_OPERATOR,
               CalculationState.CLOSING_BRACKET, CalculationState.FINISH});

   }};

    public CalculationState[] getPossibleTransitions(CalculationState state){

        return matrix.get(state);
    }

}
