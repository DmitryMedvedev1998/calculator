package interfaces;

public interface TransitionMatrix<State extends Enum> {

    State getStartState();

    State getFinishState();

    State[] getPossibleTransitions(State state);

}