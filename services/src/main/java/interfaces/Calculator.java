package interfaces;

public interface Calculator {
    Object evaluate(String mathExpression) throws Exception;
}
