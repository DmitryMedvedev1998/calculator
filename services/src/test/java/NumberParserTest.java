import org.junit.Test;
import parser.ExpressionReader;
import parser.NumberParser;

import static junit.framework.Assert.assertEquals;

public class NumberParserTest {

    @Test
    public void testNumberParsing()
    {

        NumberParser testParser = new NumberParser();

        int parsingIndex = testParser.parse(new ExpressionReader("15+12", 0));

        assertEquals("Correct index expected", 2, parsingIndex);

    }

    @Test
    public void testEmptySpaceParsing()
    {

        NumberParser testParser = new NumberParser();

        int parsingIndex = testParser.parse(new ExpressionReader("", 0));

        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testIncorrectDataParsing()
    {

        NumberParser testParser = new NumberParser();

        int parsingIndex = testParser.parse(new ExpressionReader("Cat + Dog + 15", 0));

        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testNumberWithSpacesParsing()
    {

        NumberParser testParser = new NumberParser();

        int parsingIndex = testParser.parse(new ExpressionReader(" 15 + 12", 0));

        assertEquals("Correct index is expected", 3, parsingIndex);

    }


}
