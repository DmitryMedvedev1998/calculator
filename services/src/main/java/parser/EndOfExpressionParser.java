package parser;

import exceptions.CalculationException;
import interfaces.MathExpressionParser;

public class EndOfExpressionParser implements MathExpressionParser {

    public static void clearAllData()
    {

        StackStorage.operandStack.clear();
        StackStorage.operatorStack.clear();

        OpeningBracketParser.setBracketCounterToZero();


    }

    @Override
    public int parse(ExpressionReader reader) throws CalculationException {

     if(reader.endOfExpression()){

         if(OpeningBracketParser.getBracketCounter() != 0)
         {
             clearAllData();

             throw new CalculationException("Opening brackets without matching closing brackets", 0);

         }

         while(!StackStorage.operatorStack.isEmpty()){

             BinaryOperatorParser.EvaluateTopOperator();

         }

         ResultOfCalculations.setResult(StackStorage.operandStack.peek());

         return reader.getIndex();
    }
     else
     {
         return -1;
     }
    }

}
