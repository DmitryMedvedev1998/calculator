package operators;

import java.math.BigDecimal;


public class PlusOperator implements BinaryOperator {

    private final int priority;

    public PlusOperator(int priority)
    {

        this.priority = priority;

    }
    public BigDecimal evaluate(BigDecimal leftOperand, BigDecimal rightOperand) {

        return leftOperand.add(rightOperand);

    }

    public int getPriority() {

        return priority;
    }
}
