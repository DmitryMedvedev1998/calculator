package operators;

import java.math.BigDecimal;


public class MinusOperator implements BinaryOperator {

    private final int priority;

    public MinusOperator(int priority)
    {

        this.priority = priority;

    }

    public BigDecimal evaluate(BigDecimal leftOperand, BigDecimal rightOperand) {

         return leftOperand.subtract(rightOperand);

    }

    public int getPriority() {
        return priority;
    }
}
