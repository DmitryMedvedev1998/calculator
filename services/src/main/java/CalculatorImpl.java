import exceptions.CalculationException;
import interfaces.StateRecognizer;
import interfaces.TransitionMatrix;
import parser.EndOfExpressionParser;
import parser.OpeningBracketParser;
import parser.ResultOfCalculations;

import java.math.BigDecimal;


public class CalculatorImpl extends AbstractFiniteStateMachine  implements interfaces.Calculator {

        public Object evaluate (String mathExpression) throws Exception {
            return run(new CalculationContext(mathExpression));
        }

    @Override
    TransitionMatrix getTransitionMatrix() {
        return new CalculationMatrix();
    }

    @Override
    StateRecognizer getStateRecognizer() {
      return new CalculationStateRecognizer();
    }

    @Override
    protected void deadlock(int index) throws CalculationException {

        EndOfExpressionParser.clearAllData();

        throw new CalculationException("Unexpected sequence of symbols", index);

    }


    BigDecimal getResult(){

        return ResultOfCalculations.getResult();

    }
}
