package parser;

public class ExpressionReader {

    private final String expression;
    private int index = 0;

    public ExpressionReader(String expression, int index) {

        this.expression = expression;
        this.index = index;

    }

    public int getIndex(){

        return this.index;

    }

    public void incIndex()
    {

        this.index++;

    }

    public boolean endOfExpression(){

        return index >= expression.length();

    }

    public String getRemainingExpression() {

      return expression.substring(index);

    }

    public String getNextSymbol()
    {
        if(!endOfExpression())
        {
        return expression.substring(index, index + 1);
        }

        else{

           return null;

        }
    }
}
