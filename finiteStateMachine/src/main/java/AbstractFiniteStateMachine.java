import exceptions.FiniteStateMachineException;
import interfaces.StateMachineContext;
import interfaces.StateRecognizer;
import interfaces.TransitionMatrix;

abstract public class AbstractFiniteStateMachine <State extends Enum, Matrix extends TransitionMatrix<State>,
        Recognizer extends StateRecognizer>{

    public Object run(StateMachineContext context) throws Exception {
        final Matrix transitionMatrix = getTransitionMatrix();
        final Recognizer stateRecognizer = getStateRecognizer();
        State currentState = transitionMatrix.getStartState();

        while(currentState != transitionMatrix.getFinishState())
        {
            final State[] transitions = transitionMatrix.getPossibleTransitions(currentState);

             for(State possibleState:transitions)
             {
                  if(stateRecognizer.accept(context, possibleState))
                  {

                     currentState = possibleState;

                     break;

                  }

                 if(possibleState == transitions[transitions.length - 1])
                 {

                     deadlock(stateRecognizer.getStringIndex());

                 }
             }
        }

        return getResult();
}

    protected abstract void deadlock(int index) throws FiniteStateMachineException;

    abstract Object getResult();
   abstract Matrix getTransitionMatrix();
    abstract Recognizer getStateRecognizer();
}