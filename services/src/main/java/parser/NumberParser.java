package parser;

import interfaces.MathExpressionParser;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;


public class NumberParser implements MathExpressionParser {

    NumberFormat NUMBER_FORMAT = new DecimalFormat("0.0");

    @Override
    public int parse(ExpressionReader reader){

        if(reader.endOfExpression())
        {
            return -1;
        }

        while(reader.getNextSymbol().equals(" "))
        {
            reader.incIndex();

        }
        ParsePosition parsePosition = new ParsePosition(0);

        Number parseResult = NUMBER_FORMAT.parse(reader.getRemainingExpression(), parsePosition);

        if(parseResult != null){

        BigDecimal operand = new BigDecimal(parseResult.toString());

        StackStorage.operandStack.push(operand);

        return reader.getIndex() + parsePosition.getIndex();

        }

        else
        {

          return -1;

        }

    }
}
