package parser;

import exceptions.CalculationException;
import interfaces.MathExpressionParser;


public class ClosingBracketParser implements MathExpressionParser {

    @Override
    public int parse(ExpressionReader reader) throws CalculationException {

        if(reader.endOfExpression())
        {
            return -1;
        }

        if(reader.getNextSymbol().equals(")"))
        {

        if(OpeningBracketParser.getBracketCounter() != 0)
        {

            if(OpeningBracketParser.getOperatorCounter(OpeningBracketParser.getBracketCounter() - 1) == 0)
            {

                OpeningBracketParser.decrementBracketCounter();

            }

            else{

                for(int i = 0; i < OpeningBracketParser.getOperatorCounter(OpeningBracketParser.getBracketCounter() - 1);
                    i++)
                {

                    BinaryOperatorParser.EvaluateTopOperator();

                    OpeningBracketParser.decrementOperatorCounter(OpeningBracketParser.getBracketCounter() - 1);

                }

                OpeningBracketParser.decrementBracketCounter();

            }

        }

        else
        {
            EndOfExpressionParser.clearAllData();

            throw new CalculationException("Opening bracket expected", reader.getIndex());

        }

            return reader.getIndex() + 1;

        }

        else
        {

              return -1;

        }


    }
}
