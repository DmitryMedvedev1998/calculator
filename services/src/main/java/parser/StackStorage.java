package parser;

import operators.BinaryOperator;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Deque;


public class StackStorage {

    public final static Deque <BigDecimal> operandStack = new ArrayDeque();
    public final static Deque <BinaryOperator> operatorStack = new ArrayDeque();
}
