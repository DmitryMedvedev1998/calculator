import interfaces.StateMachineContext;

public class CalculationContext implements StateMachineContext {

    private final String expression;

    public CalculationContext(String expression)
    {

       this.expression = expression;

    }

    public String getExpression(){

     return this.expression;

    }
}
