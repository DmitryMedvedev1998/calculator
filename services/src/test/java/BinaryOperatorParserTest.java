import org.junit.Test;
import parser.EndOfExpressionParser;
import parser.ExpressionReader;
import parser.BinaryOperatorParser;

import static junit.framework.Assert.assertEquals;

public class BinaryOperatorParserTest {


    @Test
    public void testBinaryOperatorParsing()
    {

        BinaryOperatorParser testParser = new BinaryOperatorParser();

        int parsingIndex = testParser.parse(new ExpressionReader("15+12", 2));

        EndOfExpressionParser.clearAllData();

        assertEquals("Correct index expected", 3, parsingIndex);

    }

    @Test
    public void testEmptySpaceParsing()
    {

        BinaryOperatorParser testParser = new BinaryOperatorParser();

        int parsingIndex = testParser.parse(new ExpressionReader("", 0));


        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testIncorrectDataParsing()
    {

        BinaryOperatorParser testParser = new BinaryOperatorParser();

        int parsingIndex = testParser.parse(new ExpressionReader("Cat + Dog + 15", 0));

        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testNumberWithSpacesParsing()
    {

        BinaryOperatorParser testParser = new BinaryOperatorParser();

        int parsingIndex = testParser.parse(new ExpressionReader(" 15 + 12", 3));

        EndOfExpressionParser.clearAllData();

        assertEquals("Correct index is expected", 5, parsingIndex);

    }


}

