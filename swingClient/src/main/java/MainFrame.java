import exceptions.CalculationException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainFrame extends JFrame {

    public MainFrame() {

       initUI();

    }

    private void initUI() {

        final JPanel panel = new JPanel();

        getContentPane().add(panel);

        panel.setLayout(null);

        final JButton calculateButton = new JButton("Calculate");


        calculateButton.setBounds(120, 80, 100, 30);

        final JTextField expressionField = new JTextField(20);

        expressionField.setBounds(20, 40, 200, 20);

        final JTextField resultField = new JTextField(20);

        resultField.setBounds(250, 40, 100, 20);

        final JLabel expression = new JLabel("Put your expression here:");

        expression.setBounds(20, 10, 200, 20);

        final JLabel result = new JLabel("Result:");

        result.setBounds(250, 10, 100, 20);

        panel.add(calculateButton);
        panel.add(expressionField);
        panel.add(resultField);
        panel.add(expression);
        panel.add(result);

        setTitle("Calculator");
        setSize(400, 400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        final CalculatorImpl resultingInstance = new CalculatorImpl();

        calculateButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {

                try {

                    resultField.setText(resultingInstance.evaluate(expressionField.getText()).toString());

                } catch (CalculationException e) {

                    JOptionPane.showMessageDialog(panel, e.getMessage(),
                            "Error", JOptionPane.ERROR_MESSAGE);

                    expressionField.requestFocus();

                    expressionField.setCaretPosition(e.getErrorIndex());

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }



        });

    }

    private void enableConsoleMode() throws Exception {

        final CalculatorImpl resultingInstance = new CalculatorImpl();
        BufferedReader messageReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter expression:");

        try {

            System.out.println(resultingInstance.evaluate(messageReader.readLine()));

        } catch (CalculationException e) {

            System.out.println(e.getMessage());
        }

    }

    public static void main(String[] args) throws Exception {


        if(args.length != 0 && args[0].equals("Enable_console"))
        {
            MainFrame mainFrame = new MainFrame();
            mainFrame.enableConsoleMode();

        }

        else
        {

            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {

                    MainFrame mainFrame = new MainFrame();
                    mainFrame.setVisible(true);

                }
            });
            }
    }

}
