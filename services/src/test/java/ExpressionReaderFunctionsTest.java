import org.junit.Test;
import parser.ExpressionReader;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;

public class ExpressionReaderFunctionsTest {

    ExpressionReader testReader = new ExpressionReader("15+50", 0);

    @Test
     public void testGettingIndex()
    {
        int index = testReader.getIndex();

        assertEquals("Correct index expected", 0, index);

    }

    @Test
    public void testIncrementingIndex()
    {
        testReader.incIndex();

        int index = testReader.getIndex();

        assertEquals("Correct index expected", 1, index);

    }

    @Test
    public void testEndOfExpression()
    {
        ExpressionReader reader = new ExpressionReader("15", 2);

        boolean testEndOfExpression = reader.endOfExpression();

        assertEquals("Expression end expected", true, testEndOfExpression);

    }

    @Test
    public void testGettingRemainingExpression()
    {
        testReader.incIndex();

        String testSubstring = testReader.getRemainingExpression();

        assertEquals("Correct remaining expression expected", "5+50", testSubstring);

    }

    @Test
    public void testGettingNextSymbol()
    {
        testReader.incIndex();

        String testSubstring = testReader.getNextSymbol();

        assertEquals("Correct remaining expression expected", "5", testSubstring);

    }

}
