package exceptions;

public class CalculationException extends FiniteStateMachineException {

    private int errorIndex;

    private String message;

    public CalculationException(String message, int errorIndex)
    {

        this.message = message;
        this.errorIndex = errorIndex;

    }

    public String getMessage()
    {

        return this.message;

    }

    public int getErrorIndex(){

        return this.errorIndex;

    }


}
