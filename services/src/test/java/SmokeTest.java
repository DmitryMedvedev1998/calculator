import exceptions.CalculationException;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;

public class SmokeTest {

    CalculatorImpl testInstance = new CalculatorImpl();

    @Test
    public void testEmptyExpression() throws Exception {



         try {

             testInstance.evaluate("+");

         }
         catch (CalculationException e) {

             assertEquals("Error index is incorrect", 0, e.getErrorIndex());

         }

    }

    @Test
    public void testSingleNumber() throws Exception
    {
        final BigDecimal result = new BigDecimal(testInstance.evaluate("10").toString());

        assertEquals("Single number expected", new BigDecimal(10), result);

    }

    @Test
    public void testExcessOpeningBracket() throws Exception
    {
        try {

            testInstance.evaluate("((10+12)");

        }
        catch (CalculationException e) {

            assertEquals("Error index is incorrect", 0, e.getErrorIndex());

        }

    }

    @Test
    public void testExcessClosingBracket() throws Exception
    {
        try {

            testInstance.evaluate("(10+12))");

        }
        catch (CalculationException e) {

            assertEquals("Error index is incorrect", 7, e.getErrorIndex());

        }

    }

    @Test
    public void testExpressionWithSpaces() throws Exception
    {
        final BigDecimal result = new BigDecimal(testInstance.evaluate("10 +15").toString());

        assertEquals("Correct result expected", new BigDecimal(25), result);



    }

    @Test
    public void testExpressionWithIncorrectSymbols() throws Exception
    {
        try {

            testInstance.evaluate("(10+cat12)");

        }
        catch (CalculationException e) {

            assertEquals("Error index is incorrect", 4, e.getErrorIndex());

        }
    }

   @Test
    public void testExpressionWithIncorrectSequenceOfSymbols() throws Exception
    {
        try {

            testInstance.evaluate("10++12");

        }
        catch (CalculationException e) {

            assertEquals("Error index is incorrect", 3, e.getErrorIndex());

        }
    }

    @Test
    public void testExpressionWithDifferentPriorityOfOperators() throws Exception
    {

        final BigDecimal result = new BigDecimal(testInstance.evaluate("10*12+15").toString());

        assertEquals("Correct result expected", new BigDecimal(135), result);

    }

    @Test
    public void testCorrectWorkOfBrackets() throws Exception
    {

        final BigDecimal result = new BigDecimal(testInstance.evaluate("10*(12+15)").toString());

        assertEquals("Correct result expected", new BigDecimal(270), result);

    }

}
