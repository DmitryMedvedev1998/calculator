package operators;

import java.util.HashMap;
import java.util.Map;

public class BinaryOperatorList {

    public static Map<String, operators.BinaryOperator> operatorList = new HashMap<String, operators.BinaryOperator>(){{

       put("+", new PlusOperator(0));
       put("-", new MinusOperator(0));
       put("*", new MultiplyOperator(1));
       put("/", new DivideOperator(1));

    }};
}
