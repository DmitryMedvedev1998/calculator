package operators;

import java.math.BigDecimal;


public class MultiplyOperator implements BinaryOperator {

    private final int priority;

    public MultiplyOperator(int priority)
    {
        this.priority = priority;
    }

    public BigDecimal evaluate(BigDecimal leftOperand, BigDecimal rightOperand) {

        return leftOperand.multiply(rightOperand);

    }

    public int getPriority() {
        return priority;
    }
}
