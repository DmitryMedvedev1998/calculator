import exceptions.CalculationException;
import interfaces.MathExpressionParser;
import interfaces.StateRecognizer;
import parser.*;

import java.util.HashMap;
import java.util.Map;


public class CalculationStateRecognizer implements StateRecognizer<CalculationState, CalculationContext> {

    private int stringIndex = 0;

    public int getStringIndex()
    {

        return this.stringIndex;

    }

    @Override
    public boolean accept(CalculationContext calculationContext, CalculationState possibleState) throws CalculationException {

        Map<CalculationState, MathExpressionParser> parsers = new HashMap<CalculationState, MathExpressionParser>() {{
        put(CalculationState.NUMBER, new NumberParser());
        put(CalculationState.BINARY_OPERATOR, new BinaryOperatorParser());
        put(CalculationState.OPENING_BRACKET, new OpeningBracketParser());
        put(CalculationState.CLOSING_BRACKET, new ClosingBracketParser());
        put(CalculationState.FINISH, new EndOfExpressionParser());
        }
        };




        int parseResult = parsers.get(possibleState).parse(new ExpressionReader(calculationContext.getExpression(),
                    stringIndex));


        if(parseResult == -1){

            return false;
        }


        else{


        stringIndex = parseResult;

            return true;

        }
        }
    }

