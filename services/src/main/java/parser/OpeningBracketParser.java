package parser;

import interfaces.MathExpressionParser;


public class OpeningBracketParser implements MathExpressionParser {

    private static int bracketCounter = 0;

    private static int operatorCounters[] = new int[256];

    public static int getBracketCounter() {

        return bracketCounter;

    }

    public static void decrementBracketCounter() {

        OpeningBracketParser.bracketCounter--;

    }

    public static void setBracketCounterToZero() {

        OpeningBracketParser.bracketCounter = 0;

    }

    public static void incOperatorCounter(int index){

        operatorCounters[index]++;

    }

    public static void decrementOperatorCounter(int index){

        operatorCounters[index]--;

    }

    public static int getOperatorCounter(int index){

        return operatorCounters[index];

    }

    @Override
    public int parse(ExpressionReader reader){

        if(reader.endOfExpression())
        {
            return -1;
        }

        if(reader.getNextSymbol().equals("("))
        {

            bracketCounter++;

            return reader.getIndex() + 1;
        }

        else
        {

            return -1;

        }

    }
}
