import operators.DivideOperator;
import operators.MinusOperator;
import operators.MultiplyOperator;
import operators.PlusOperator;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;

public class BinaryOperatorEvaluatingTest {

    @Test
    public void testPlusOperator()
    {

        PlusOperator testOperator = new PlusOperator(0);

        BigDecimal evaluationResult = testOperator.evaluate(new BigDecimal(15), new BigDecimal(14));

        assertEquals("Correct result of calculations expected", new BigDecimal(29), evaluationResult);

    }

    @Test
    public void testMinusOperator()
    {

        MinusOperator testOperator = new MinusOperator(0);

        BigDecimal evaluationResult = testOperator.evaluate(new BigDecimal(15), new BigDecimal(14));

        assertEquals("Correct result of calculations expected", new BigDecimal(1), evaluationResult);

    }

    @Test
    public void testMultiplyOperator()
    {

        MultiplyOperator testOperator = new MultiplyOperator(0);

        BigDecimal evaluationResult = testOperator.evaluate(new BigDecimal(15), new BigDecimal(3));

        assertEquals("Correct result of calculations expected", new BigDecimal(45), evaluationResult);

    }

    @Test
    public void testDivideOperator()
    {

        DivideOperator testOperator = new DivideOperator(0);

        BigDecimal evaluationResult = testOperator.evaluate(new BigDecimal(15), new BigDecimal(5));

        assertEquals("Correct result of calculations expected", 3, evaluationResult.intValueExact());

    }

    @Test
    public void testDivideOperatorIncorrectDivision()
    {

        DivideOperator testOperator = new DivideOperator(0);

        try{

        testOperator.evaluate(new BigDecimal(15), new BigDecimal(0));

        }
        catch (ArithmeticException e)
        {
           assertEquals("Exception expected", "/ by zero", e.getMessage());

        }

    }

}
