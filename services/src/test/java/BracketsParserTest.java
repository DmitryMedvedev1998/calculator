import exceptions.CalculationException;
import org.junit.Test;
import parser.ClosingBracketParser;
import parser.EndOfExpressionParser;
import parser.ExpressionReader;
import parser.OpeningBracketParser;

import static junit.framework.Assert.assertEquals;

public class BracketsParserTest {

    @Test
    public void testOpeningBracketParsing()
    {

        OpeningBracketParser testParser = new OpeningBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("(15+12)", 0));

        assertEquals("Correct index expected", 1, parsingIndex);

    }

    @Test
    public void testClosingBracketParsing() throws CalculationException {

        ClosingBracketParser testParser = new ClosingBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("(15+12)", 6));

        EndOfExpressionParser.clearAllData();

        assertEquals("Correct index expected", 7, parsingIndex);

    }

    @Test
    public void testOpeningBracketEmptySpaceParsing()
    {

        OpeningBracketParser testParser = new OpeningBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("", 0));


        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testClosingBracketEmptySpaceParsing() throws CalculationException {

        ClosingBracketParser testParser = new ClosingBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("", 0));


        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testOpeningBracketIncorrectDataParsing()
    {

        OpeningBracketParser testParser = new OpeningBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("Cat + Dog + 15", 0));

        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }

    @Test
    public void testClosingBracketIncorrectDataParsing() throws CalculationException {

        ClosingBracketParser testParser = new ClosingBracketParser();

        int parsingIndex = testParser.parse(new ExpressionReader("Cat + Dog + 15", 0));

        assertEquals("Nothing should be parsed, returning -1", -1, parsingIndex);

    }
}



