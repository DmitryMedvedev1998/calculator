package exceptions;

public abstract class FiniteStateMachineException extends Exception {


    abstract int getErrorIndex();

}
