package parser;

import interfaces.MathExpressionParser;
import operators.BinaryOperator;
import operators.BinaryOperatorList;

import java.math.BigDecimal;

public class BinaryOperatorParser implements MathExpressionParser {

    public static void EvaluateTopOperator(){

        BigDecimal rightOperand = StackStorage.operandStack.pop();
        BigDecimal leftOperand = StackStorage.operandStack.pop();

        StackStorage.operandStack.push(StackStorage.operatorStack.pop().evaluate(leftOperand, rightOperand));

    }

    @Override
     public int parse(ExpressionReader reader) {

        if(reader.endOfExpression())
        {
            return -1;
        }

        while(reader.getNextSymbol().equals(" "))
        {
            reader.incIndex();

        }

        BinaryOperator currentBinaryOperator = BinaryOperatorList.operatorList.get(reader.getNextSymbol());

        if(currentBinaryOperator != null){

            if(OpeningBracketParser.getBracketCounter() != 0)
            {

                StackStorage.operatorStack.push(currentBinaryOperator);

                OpeningBracketParser.incOperatorCounter(OpeningBracketParser.getBracketCounter() - 1);

            }

            else if(StackStorage.operatorStack.isEmpty() || StackStorage.operatorStack.peek().getPriority() <
                    currentBinaryOperator.getPriority())
            {
                StackStorage.operatorStack.push(currentBinaryOperator);
            }
            else
            {
                EvaluateTopOperator();
                StackStorage.operatorStack.push(currentBinaryOperator);
            }

            return reader.getIndex() + 1;

        }

        else
        {
            return -1;
        }
    }
}
