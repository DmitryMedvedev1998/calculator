package operators;

import java.math.BigDecimal;

public interface BinaryOperator {

    int getPriority();

    BigDecimal evaluate(BigDecimal leftOperand, BigDecimal rightOperand);
}

