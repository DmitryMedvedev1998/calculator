package interfaces;

import exceptions.CalculationException;
import parser.ExpressionReader;

public interface MathExpressionParser {
int parse(ExpressionReader reader) throws CalculationException;
}
