package operators;

import java.math.BigDecimal;

public class DivideOperator implements BinaryOperator {

    private final int priority;

    public DivideOperator(int priority)
    {

        this.priority = priority;

    }

    public BigDecimal evaluate(BigDecimal leftOperand, BigDecimal rightOperand) {

        return leftOperand.divide(rightOperand, 8, BigDecimal.ROUND_HALF_UP);

    }

    public int getPriority() {
        return priority;
    }
}
