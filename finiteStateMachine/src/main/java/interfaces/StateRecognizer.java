package interfaces;

import exceptions.FiniteStateMachineException;

public interface StateRecognizer<State extends Enum, Context> {

    int getStringIndex();

    boolean accept (Context context, State possibleState) throws FiniteStateMachineException;
}